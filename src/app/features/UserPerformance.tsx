import { useEffect } from "react"
import Skeleton from "react-loading-skeleton"
import withBoundary from "../../core/hoc/withBoundary"
import useUserPerformance from "../../core/hooks/useUserPerformance"
import Chart from "../components/Chart/Chart"

function UserPerformance () {
  const {editorEarnings, fetchUserPerformance} = useUserPerformance()
  

  useEffect(() => {
    fetchUserPerformance()
  }, [fetchUserPerformance])

  if (!editorEarnings)
    return <div>
      <Skeleton height={227}/>
    </div>

  return <Chart
    title="Média de performance nos últimos 12 meses"
    data={editorEarnings}
  />
}

export default withBoundary(UserPerformance, 'performance')