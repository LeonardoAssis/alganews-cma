import { useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import styled from "styled-components";
import withBoundary from "../../core/hoc/withBoundary";
import useUserTopTags from "../../core/hooks/useUserTopTags";
import CircleChart from "../components/CircleChart";

function UserTopTags () {
  const { topTags, fetchTopTags } = useUserTopTags()

  useEffect(() => {
    fetchTopTags()
  }, [fetchTopTags])

  if (!topTags.length) 
    return <UserTopTagsWrapper>
      <Skeleton 
        circle
        height={88}
        width={88}/>
      <Skeleton 
        circle
        height={88}
        width={88}/>
      <Skeleton 
        circle
        height={88}
        width={88}/>
    </UserTopTagsWrapper>

  return <UserTopTagsWrapper>
    {
      topTags.map((tag, i) => {
        return <CircleChart 
          key={i}
          progress={tag.percentage}
          caption={tag.tagName}
          size={88}
          theme={i === 0 ? 'primary' : 'default'}
        />
      })
    }
  </UserTopTagsWrapper>
}

const UserTopTagsWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 32px;
`

export default withBoundary(UserTopTags, 'tags')