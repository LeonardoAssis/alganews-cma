import { mdiOpenInNew } from "@mdi/js";
import Icon from "@mdi/react";
import { format } from "date-fns";
import { Post } from "leonardoassis-sdk";
import { useState } from "react";
import { useEffect } from "react";
import { useMemo } from "react";
import { confirmAlert } from "react-confirm-alert";
import Skeleton from "react-loading-skeleton";
import { Column, usePagination, useTable } from "react-table";
import withBoundary from "../../core/hoc/withBoundary";
import usePosts from "../../core/hooks/usePosts";
import Loading from "../components/Loading";
import PostPreview from "../components/PostPreview/PostPreview";
import PostTitleAnchor from "../components/PostTitleAnchor";
import Table from "../components/Table/Table";

function PostList() {
  const [page, setPage] = useState(0);
  
  const {paginatedPosts, loading, fetchPosts} = usePosts()

  useEffect(() => {
    fetchPosts({
      page,
      size: 7,
      showAll: true,
      sort: ["createdAt", "desc"],
    })
  }, [page, fetchPosts]);

  const columns = useMemo<Column<Post.Summary>[]>(
    () => [
      {
        Header: "",
        accessor: "id",
        Cell: () => <Icon path={mdiOpenInNew} size={"14px"} color={"#9f"} />,
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>Título</div>,
        accessor: "title",
        Cell: (props) => (
          <div
            style={{
              textAlign: "left",
              display: "flex",
              alignItems: "center",
              gap: 8,
              maxWidth: 270
            }}
          >
            <img
              width={24}
              height={24}
              src={props.row.original.editor.avatarUrls.small}
              alt={props.row.original.editor.name}
              title={props.row.original.editor.name}
            />
            <PostTitleAnchor 
              title={props.value}
              href={`/posts/${props.row.original.id}`}
              onClick={e => {
                e.preventDefault();
                confirmAlert({
                  message: 'TESTE',
                  customUI: () => {
                    return <PostPreview 
                      postId={props.row.original.id}/>
                  }
                })
              }} >
              {props.value}
            </PostTitleAnchor>
          </div>
        ),
      },
      {
        Header: () => <div style={{ textAlign: "right" }}>Criação</div>,
        accessor: "createdAt",
        Cell: (props) => (
          <div style={{ textAlign: "right", fontWeight: 700 }}>
            {format(new Date(props.value), "dd/MM/yyyy")}
          </div>
        ),
      },
      {
        Header: () => (
          <div style={{ textAlign: "left" }}>Última atualização</div>
        ),
        accessor: "updatedAt",
        Cell: (props) => (
          <div
            style={{
              display: "flex",
              gap: 8,
              fontWeight: 700,
              fontFamily: '"Roboto Mono", monospace',
            }}
          >
            {format(new Date(props.value), "dd/MM/yyyy")}
          </div>
        ),
      },
      {
        id: Math.random().toString(),
        accessor: "published",
        Header: () => <div style={{ textAlign: "right" }}>Ações</div>,
        Cell: (props) => (
          <div style={{ textAlign: "right" }}>
            {props.value ? "Publicado" : "Privado"}
          </div>
        ),
      },
    ],
    []
  );

  const instance = useTable<Post.Summary>(
    {
      data: paginatedPosts?.content || [],
      columns,
      manualPagination: true,
      initialState: { pageIndex: 0 },
      pageCount: paginatedPosts?.totalPages,
    },
    usePagination
  );

  if (!paginatedPosts)
    return (
      <div>
        <Skeleton height={32} />
        <Skeleton height={40} />
        <Skeleton height={40} />
        <Skeleton height={40} />
        <Skeleton height={40} />
        <Skeleton height={40} />
        <Skeleton height={40} />
      </div>
    );

  return (
    <>
      <Loading show={loading} />
      <Table<Post.Summary> instance={instance} onPaginate={setPage} />
    </>
  );
}

export default withBoundary(PostList, "posts");
