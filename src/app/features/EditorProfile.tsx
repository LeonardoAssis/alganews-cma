import { getEditorDescription } from "leonardoassis-sdk"
import { transparentize } from "polished"
import { useEffect } from "react"
import { useParams } from "react-router"
import styled from "styled-components"
import useSingleEditor from "../../core/hooks/useSingleEditor"
import FieldDescriptor from "../components/FieldDescriptor/FieldDescriptor"
import ProgressBar from "../components/ProgressBar/ProgressBar"
import ValueDescriptor from "../components/ValueDescriptor/ValueDescriptor"

interface EditorProfileProps {
  hidePersonalData?: boolean
}

export default function EditorProfile (props: EditorProfileProps) {
  const params = useParams<{ id: string }>()
  const {editor, fetchEditor} = useSingleEditor()
  
  
  useEffect(() => {
    fetchEditor(Number(params.id))
  }, [params.id, fetchEditor])

  if (!editor)
    return null

  return <EditorProfileWrapper>
    <EditorHeadline>
      <Avatar src={editor?.avatarUrls.small}/>

      <div style={{fontSize:'18px', fontWeight: 'bold'}}>
        {editor?.name}
      </div>
      <div>{getEditorDescription(new Date(editor.createdAt))}</div>
    </EditorHeadline>
    <EditorBodyWrapper>
      <div style={{gap: '16px', display: 'flex', flexDirection:'column'}}>
        <Biography>{
          editor.bio
        }</Biography>

        <Skills>
          {
            editor.skills?.map((skill) => {
              return <ProgressBar 
                  key={skill.name}
                  progress={skill.percentage} 
                  title={skill.name} 
                  theme="primary"/>
            })
          }
        </Skills>
      </div>

      <Contacts>
        <FieldDescriptor 
          title="cidade"
          description={editor.location.city}
        />

        <FieldDescriptor 
          title="Estado"
          description={editor.location.state}
        />

        {
          !props.hidePersonalData &&
          <>
            <FieldDescriptor 
              title="telefone"
              description="+55 27 91234-5678"
            />

            <FieldDescriptor 
              title="email"
              description="ana.castillo@redacao.algacontent.com"
            />

            <FieldDescriptor 
              title="data de nascimento"
              description="26 de Dezembro de 1997 (22anos)"
            />
          </>
        }
        
      </Contacts>
    </EditorBodyWrapper>
      
    {
      !props.hidePersonalData &&

      <EditorEarnings>
        <ValueDescriptor 
          value={20345}
          description="palavras nesta semana"
          color="default"
        />
        <ValueDescriptor 
          value={140432}
          description="palavras no mês"
          color="default"
        />
        <ValueDescriptor 
          value={2434423}
          description="total de palavras"
          color="default"
        />
        <ValueDescriptor 
          value={530322.02}
          description="ganhos nesta semana"
          color="primary"
          isCurrency={true}
        />
        <ValueDescriptor 
          value={530322.02}
          description="ganhos no mês"
          color="primary"
          isCurrency={true}
        />
        <ValueDescriptor 
          value={530322.02}
          description="ganhos sempre"
          color="primary"
          isCurrency={true}
        />
      </EditorEarnings>
    }

    
  </EditorProfileWrapper>

}

const EditorProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 24px;
  gap: 16px;
  border: 1px solid ${transparentize(0.9, '#274060')};
`
const EditorHeadline = styled.div`
  display: grid;
  align-items: center;
  grid-template-rows: 2;
  grid-template-columns: 48px auto;
  gap: 8px 16px;
  height: 48px;
`
const Avatar = styled.img`
  grid-row-start: 1;
  grid-row-end: 3;
  height: 48px;
  width: 48px;
  background-color: #C4C4C4;
`
const EditorBodyWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 2;
  gap: 32px;

  border-top: 1px solid ${transparentize(0.9, '#274060')};;
  padding-top: 24px;
`

const Biography = styled.p`
  font-size: 12px;
  line-height: 20px;
`

const Contacts = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 16px 0;

  >* {
    width: 100%;
  }
  &>:nth-child(1),
  &>:nth-child(2) {
    width: 50%;
  }
`

const Skills = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`

const EditorEarnings = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr 1fr;
  gap: 24px;
`