import { useEffect } from "react"
import usePageTitle from "../../core/hooks/usePageTitle"
import usePosts from "../../core/hooks/usePosts"
import PostList from "../features/PostList"
import UserEarnings from "../features/UserEarnings"
import UserPerformance from "../features/UserPerformance"
import UserTopTags from "../features/UserTopTags"
import DefaultLayout from "../layouts/Default"

export default function Home () {
  usePageTitle('Home')
  const { fetchPosts } = usePosts();

  useEffect(() => {
    fetchPosts({ page: 1})
  }, [fetchPosts])

  return <DefaultLayout>
    
    <div style={{display: 'grid', gridTemplateColumns: '1fr 1fr', alignItems: 'center', gap: '32px', padding: '32px'}}>
      <UserTopTags />
      <UserEarnings />
    </div>

    <UserPerformance />
    <PostList />
  </DefaultLayout>
}