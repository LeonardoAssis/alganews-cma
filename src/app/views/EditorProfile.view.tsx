import usePageTitle from "../../core/hooks/usePageTitle";
import ErrorBoundary from "../components/ErrorBoundary/ErrorBoundary";
import EditorProfile from "../features/EditorProfile";
import DefaultLayout from "../layouts/Default";

function EditorProfileView () {
  usePageTitle('Editor')

  return <DefaultLayout>
    <ErrorBoundary>
      <EditorProfile hidePersonalData={true}/>
    </ErrorBoundary>
  </DefaultLayout>
}

export default EditorProfileView