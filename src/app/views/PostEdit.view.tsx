import { useParams } from "react-router"
import usePageTitle from "../../core/hooks/usePageTitle"
import PostForm from "../features/PostForm"
import DefaultLayout from "../layouts/Default"


export default function PostEditView () {
  const params = useParams<{ id: string }>()
  usePageTitle('Editores')

  return <DefaultLayout>
    <PostForm postId={Number(params.id)}/>
  </DefaultLayout>
}