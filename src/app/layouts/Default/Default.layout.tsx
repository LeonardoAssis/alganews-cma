import confirm from '../../../core/utils/confirm'
import info from '../../../core/utils/info'
import Logo from '../../components/Logo'
import NavBar from '../../components/NavBar/NavBar'
import SessionController from '../../components/SessionController'
import * as D from './Default.layout.styles'

interface DefaultLayoutProps {
  children: React.ReactNode
}

function DefaultLayout (props: DefaultLayoutProps) {
  return <D.Wrapper>
    <D.Header>
      <Logo />
    </D.Header>
    <D.Main>
      <D.Navigation>
        <NavBar />
      </D.Navigation>
      <D.FeaturedContent>{props.children}</D.FeaturedContent>
      <D.Aside>
        <SessionController
          name="Leonardo Souza de Assis"
          description="programador"
          onLogout={() => {
            confirm({
              title: "Você quer deslogar?",
              onConfirm: () => {
                info({
                  title: 'Você foi deslogado',
                  description: 'Você será redirecionado para a página de login'
                })
                console.log('executando')
              }
            })
          }}/>
      </D.Aside>
    </D.Main>
  </D.Wrapper>
}

export default DefaultLayout