import { useEffect } from "react";
import { useState } from "react";
import styled from "styled-components";
import withBoundary from "../../../core/hoc/withBoundary";
import MarkdownEditor from "../MarkdownEditor";
import NoData from "../Nodata/NoData";
import { Heading2 } from "../Typography/Heading.styles";
import Loading from "../Loading";
import Button from "../Button/Button";
import info from "../../../core/utils/info";
import confirm from "../../../core/utils/confirm";
import modal from "../../../core/utils/moda";
import { PostService } from "leonardoassis-sdk";
import usePostPreview from "../../../core/hooks/usePostPreview";

export interface PostPreviewProps {
  postId: number
}

function PostPreview (props: PostPreviewProps) {
  const {post, fetchPostPreview} = usePostPreview()
  const [loading, setLoading] = useState(false)

  async function publishPost() {
    await PostService.publishExistingPost(props.postId)
    info({
      title: 'Post publicado',
      description: 'você publicou o post com sucesso'
    })
  }

  function reopenModal () {
    modal({
      children: <PostPreview postId={props.postId} />
    })
  }

  useEffect(() => {
    fetchPostPreview(props.postId)
      .finally(() => setLoading(false))
  }, [fetchPostPreview, props.postId])

  if (loading)
    return <Loading show />

  if (!post)
    return <Wrapper>
      <NoData />
    </Wrapper>

  return <Wrapper>
      <Header>
        <Heading2>{post.title}</Heading2>
        <ButtonsWrapper>
          <Button 
            label={'Publicar'}
            variant={'danger'}
            disabled={post.published}
            onClick={() => {
              confirm({
                title: 'Publicar o post?',
                onConfirm: publishPost,
                onCancel: reopenModal,
              })
            }}/>
          <Button 
            label={'Editar'}
            variant={'primary'}
            disabled={post.published}
            onClick={() => {
              console.log("PostID before: " + props.postId)
              window.location.pathname = `/posts/editar/${props.postId}`
            }}/>
        </ButtonsWrapper>
      </Header>

      <PostPreviewImage src={post.imageUrls.medium}/>

      <MarkdownEditor 
        readOnly={true}
        value={post.body}/>
  </Wrapper>
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 24px;
  width: 655px;
  max-height: 70vh;
  overflow-y: scroll;
  background-color: #f3f8fa;
  border: 1px solid #ccc;
  padding: 24px;
`

const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  gap: 24px;
`

const ButtonsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 8px;
  align-items: center;
`

const PostPreviewImage = styled.img`
  height: 240px;
  width: 100%;
  object-fit: cover;
`

export default withBoundary(PostPreview)