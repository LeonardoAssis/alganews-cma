import { useEffect, useState } from 'react'
import * as CC from './CircleChart.styles'

export interface CircleChartProps {
  size: number
  progress: number
  caption?: string
  theme?: 'default' | 'primary'
  strokeWidht?: number
}

export default function CircleChart (props: CircleChartProps) {
  const getThemeColor = () => 
    props.theme === 'primary' ? '#09f' : '#274060'

  const THEME = getThemeColor()
  const STROKE_WIDTH = props.strokeWidht || 8
  const STROKE_COLOR = THEME

  const CENTER = props.size / 2
  const RADIUS = props.size / 2 - STROKE_WIDTH / 2
  const CIRCUNFERENCE = 2 * Math.PI *  RADIUS

  const [offset, setOffset] = useState(CIRCUNFERENCE)

  useEffect(() => {
    const progressOffset = ((100 - props.progress) / 100) * CIRCUNFERENCE
    setOffset(progressOffset)
  }, [setOffset, CIRCUNFERENCE, props.progress, offset])

  return <CC.Wrapper>
    <CC.SvgWrapper style={{ width: props.size, height: props.size }}>
      <CC.Svg width={props.size} height={props.size}>
        <CC.CircleBG
          cy={CENTER}
          cx={CENTER}
          r={RADIUS}/>
          <CC.Circle
            fill="none"
            cy={CENTER}
            cx={CENTER}
            r={RADIUS}
            stroke={STROKE_COLOR}
            strokeWidth={STROKE_WIDTH}
            strokeDasharray={CIRCUNFERENCE}
            strokeDashoffset={offset}/>
      </CC.Svg>
      <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
      <CC.Percentage>
        {Math.ceil(props.progress)}%
      </CC.Percentage>
      {
        props.caption && 
          <CC.Caption>
            { props.caption }
          </CC.Caption>
      }
      </div>
    </CC.SvgWrapper>
  </CC.Wrapper>
}