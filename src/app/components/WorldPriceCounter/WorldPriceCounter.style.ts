import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;

  background: transparent;
  gap: 10px;
  padding: 8px;
`

export const WorldCounter = styled.span`
  text-transform: uppercase;
  color: #274060;
`

export const PricePreview = styled.span`
  color: #09F;
`