import * as W from './WorldPriceCounter.style'

export interface WorldPriceCounterProps {
  wordsCount: number
  pricePerWord: number
}

function WorldPriceCounter (props: WorldPriceCounterProps) {

  if (props.wordsCount < 0)
    throw Error('A quantidade de palavras não pode ser negativa')

  return <W.Wrapper>
    <W.WorldCounter>{ props.wordsCount } palavras</W.WorldCounter>
    <W.PricePreview>{(props.wordsCount * props.pricePerWord).toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
      maximumFractionDigits: 2
    })}</W.PricePreview>
  </W.Wrapper>
}

export default WorldPriceCounter