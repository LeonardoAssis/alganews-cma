import styled from "styled-components";

export const Wrapper = styled.div<{}>`
  display: flex;
  flex-direction: column;
  gap: 4px;
  color: #274060;;

  .Title {
    font-size: 12px;
    font-weight: bold;
    text-transform: lowercase;
  }

  .Description{
    font-size: 14px;
  }
`