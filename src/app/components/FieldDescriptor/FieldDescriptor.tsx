import * as F from './FieldDescriptor.styles'

export interface FieldDescriptorProps {
  title: string,
  description: string
}

export default function FieldDescriptor(props: FieldDescriptorProps) {
  return <F.Wrapper>
    <label className="Title">{props.title}</label>
    <label className="Description">{props.description}</label>
  </F.Wrapper>
}