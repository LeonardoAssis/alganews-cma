import styled from "styled-components"
import Heading from "../Typography/Heading"
import Button from "../Button/Button"


export interface ConfirmProps {
  title: string
  onConfirm: () => any
  onCancel: () => any
}

export default function Confirm (props: ConfirmProps) {
  return <ConfirmWrapper>
    <Heading level= {3} >{props.title}</Heading>
    <ButtonsWrapper>
      <Button variant="danger" label="Não" onClick={props.onCancel}/>
      <Button variant="primary" label="Sim" onClick={props.onConfirm}/>
    </ButtonsWrapper>

  </ConfirmWrapper>
}

export const ConfirmWrapper = styled.div<{}>`
  padding: 24px;
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  width: 229px;
  background-color: #F3F8FA;
`

export const ButtonsWrapper = styled.div<{}>`
  display: flex;
  flex-direction: row;
  text-align: center;
  justify-content: center;
  padding: 10px;
  gap: 10px;
`