import { transparentize } from "polished";
import styled from "styled-components";

export const Wrapper = styled.div`
  height: 183px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
  gap: 12px;
  background-color: transparent;
  border: 1px solid ${transparentize(0.9, '#274060')};
  color: #274060;

  &:hover {
    border: 2px solid #B50FBF;
  }
`

export const Avatar = styled.img`
  height: 47px;
  width: 47px;
`

export const Name = styled.h2`
  font-size: 18px;
  font-weight: 500;
  text-align: center;

  width: 100%;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`