import React, { ButtonHTMLAttributes, DetailedHTMLProps } from 'react'
import * as B from './Button.styles'

export interface ButtonProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>
{
  variant: 'danger' | 'text' | 'primary'
  label: string
  children?: React.ReactNode
}

export default function Button ({label, variant, children, ref, ...props }: ButtonProps) {

  return <B.Wrapper 
    variant={variant} 
    {...props}
  >
    {label}{children}
  </B.Wrapper>
}