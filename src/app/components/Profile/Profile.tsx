import Heading from '../Typography/Heading'
import * as P from './Profile.styles'

export interface ProfileProps {
  name: string
  description: string
  editorId: number;
  avatarUrl?: string;
}

function Profile (props: ProfileProps) {
  return <P.Wrapper >
    <P.LinkWrapper to={`editores/${props.editorId}`} tabIndex={0}>
        <P.Avatar  src={props.avatarUrl} >
        </P.Avatar>
        <P.NameWrapper>
          <Heading level={3}>
            {props.name}
          </Heading>
          <span>
            {props.description}
          </span>
        </P.NameWrapper>
      
    </P.LinkWrapper>
  </P.Wrapper>
}

export default Profile