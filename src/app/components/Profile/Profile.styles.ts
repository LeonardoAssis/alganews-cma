import { transparentize } from "polished";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const Wrapper = styled.div`
  

`
export const LinkWrapper = styled(Link)`
  height: 80px;
  width: 100%;
  display: flex;
  padding: 16px;
  gap: 24px;
  background-color: transparent;
  transition: box-shadow .15s ease; 

  text-decoration: none;
  color: #274060;
  border: 1px solid #ccc; 

  &:focus,
  &:hover {
    border-color: #09f;
    outline: none;
    box-shadow: 0 0 0 5px #09f;
  };
`

export const Avatar = styled.img`
  height: 48px;
  width: 48px;
  background-color: #C4C4C4;
`

export const NameWrapper = styled.div`
  color: #274060;
  display: flex;
  flex-direction: column;
  gap: 8px;
  justify-content: right;
  align-items: right;
  font-size: 12;
  font-weight: 700;
`