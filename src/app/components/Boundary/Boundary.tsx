import { mdiAlert } from "@mdi/js";
import Icon from "@mdi/react";
import styled from "styled-components";
import Heading from "../Typography/Heading";

export interface BoundaryProps{
  title: string
  description: string
  iconSize: 'small' | 'big'
}

export default function Boundary(props: BoundaryProps) {
  return <Wrapper>
    <Icon 
      size={props.iconSize === 'small' ? '24px' : '48px'}
      path={mdiAlert}/>
    <Heading level={3}>{props.title}</Heading>
    <span style={{fontSize:'12'}}>{props.description}</span>
  </Wrapper>
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 9px;
  justify-content: center;
  align-items: center;
  color: #274060
`