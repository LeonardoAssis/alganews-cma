import logo from '../../assets/Logo.svg'

export default function Logo () {
  return <img src={logo} alt="AlgaNews CMS" />
}