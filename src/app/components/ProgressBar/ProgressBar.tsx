import * as P from './ProgressBar.styles'

export interface ProgressBarProps {
  title: string
  progress: number
  theme: 'primary' | 'secondary'
  width?: number
}

export default function ProgressBar ({title, progress, theme, width= 296}: ProgressBarProps) {
  return <P.ProgressBarWrapper 
    style={{ width: width || 'auto' }}
    title={title}
    progress={progress}
    theme={theme}
    width={width}
    >
    
    <P.TextShadow progress={progress} theme={theme}>
      {title}
    </P.TextShadow>
    <P.Concluded 
      progress={progress}
      theme={theme}
      >

      <span>{title}</span>

    </P.Concluded>
  </P.ProgressBarWrapper>
}