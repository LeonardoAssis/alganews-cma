import React from 'react';
import { Story, Meta } from '@storybook/react';

import  ProgressBar, { ProgressBarProps } from '../app/components/ProgressBar/ProgressBar'

export default {
  title: 'Example/ProgressBar',
  component: ProgressBar,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta<ProgressBarProps>;

const Template: Story<ProgressBarProps> = (args) => <ProgressBar {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  title: 'javascript',
  theme: 'primary',
  progress: 80
}