import React from 'react';
import { Story, Meta } from '@storybook/react';
import Boundary, { BoundaryProps } from '../app/components/Boundary/Boundary';



export default {
  title: 'Example/Boundary',
  component: Boundary
} as Meta;

const Template: Story<BoundaryProps> = (args) => <Boundary {...args} />;

export const Default = Template.bind({});
Default.args = {
  title: 'Erro ao recuperar componente',
  description: 'Código de erro que seja identificável pelo time de desenvolvimento',
  iconSize: 'big'
}
