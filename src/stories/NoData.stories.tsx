import React from 'react';
import { Story, Meta } from '@storybook/react';

import  NoData, {NoDataProps} from '../app/components/Nodata/NoData'

export default {
  title: 'Example/NoData',
  component: NoData,
  argTypes: {
    backgroundColor: { control: 'color' },
    onNoData: { action: 'NoData' },
    onCancel: {action: 'cancel'}
  },
} as Meta<NoDataProps>;

const Template: Story<NoDataProps> = (args) => <NoData {...args} />;

export const Default = Template.bind({});
Default.args = {
};

export const Fixed240 = Template.bind({});
Fixed240.args = {
  height: 240
};