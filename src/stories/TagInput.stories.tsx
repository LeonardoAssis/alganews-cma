import React from 'react';
import { Story, Meta } from '@storybook/react';

import  TagInput, {TagInputProps} from '../app/components/TagInput'
import { useState } from 'react';
import { Tag } from 'react-tag-input';

export default {
  title: 'Example/TagInput',
  component: TagInput
} as Meta;

const Template: Story<TagInputProps> = (args) => <TagInput {...args} />;

export const Default = Template.bind({});
Default.args = {
  tags: [
    { id: '1', text: 'JavaScript'}
  ],
  placeholder: 'Insira as tags deste post'
}

export const VariousTags = Template.bind({});
VariousTags.args = {
  tags: [
    { id: '1', text: 'JavaScript'},
    { id: '2', text: 'Java'},
    { id: '3', text: 'Ruby on Rails'},
    { id: '4', text: 'Python'},
    { id: '5', text: 'C++'},
    { id: '6', text: '.Net'},
    { id: '7', text: 'Pascal'}
  ],
  placeholder: 'Insira as tags deste post'
}

export function WorkingLiveExample() {
  const [tags, setTags] = useState<Tag[]>([])

  return <TagInput 
    placeholder="Insira as tags deste post"
    tags={tags} 
    onAdd={tag => setTags([...tags, tag])}
    onDelete={index => setTags(tags.filter((tag, i) => i !== index))}
  />
}