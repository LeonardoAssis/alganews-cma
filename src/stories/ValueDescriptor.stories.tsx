import React from 'react';
import { Story, Meta } from '@storybook/react';

import  ValueDescriptor, {ValueDescriptorProps} from '../app/components/ValueDescriptor/ValueDescriptor'

export default {
  title: 'Example/ValueDescriptor',
  component: ValueDescriptor,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta<typeof ValueDescriptor>;

const Template: Story<ValueDescriptorProps> = (args) => <ValueDescriptor {...args} />;

export const Default = Template.bind({});
Default.args = {
  color: 'default',
  description: 'ganhos no mês',
  value: 560322.02
}

export const Primary = Template.bind({});
Primary.args = {
  color: 'primary',
  description: 'ganhos no mês',
  value: 560322.02
}

export const DefaultCurrency = Template.bind({});
DefaultCurrency.args = {
  color: 'default',
  description: 'ganhos no mês',
  value: 560322.02,
  isCurrency: true
}

export const PrimaryCurrency = Template.bind({});
PrimaryCurrency.args = {
  color: 'primary',
  description: 'ganhos no mês',
  value: 560322.02,
  isCurrency: true
}