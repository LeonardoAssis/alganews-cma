import { MetricService } from "leonardoassis-sdk"
import { useCallback, useState } from "react"
import { ChartProps } from "../../app/components/Chart/Chart"
import transformEditorMonthlyEarningsIntoChartJs from "../utils/transformEditorMonthlyEarningsIntoChartJs"

export default function useUserPerformance() {
  const [editorEarnings, setEditorEarnings] = useState<ChartProps['data']>()

  const fetchUserPerformance = useCallback(async function () {
    MetricService
      .getEditorMonthlyEarnings()
      .then(transformEditorMonthlyEarningsIntoChartJs)
      .then(setEditorEarnings)
  }, [])

  return {
    editorEarnings,
    fetchUserPerformance
  }
}