import { Post, PostService } from "leonardoassis-sdk";
import { useCallback, useState } from "react";

export default function usePostPreview () {
  const [post, setPost] = useState<Post.Detailed>()

  const fetchPostPreview = useCallback(async function (postId: number) {
    PostService
      .getExistingPost(postId)
      .then(setPost)
  }, [])

  return {
    post,
    fetchPostPreview
  }
}