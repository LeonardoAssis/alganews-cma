import { Metrics, MetricService } from "leonardoassis-sdk";
import { useCallback, useState } from "react";

export default function useUserTopTags(){
  const [topTags, setTopTags] = useState<Metrics.EditorTagRatio>([])

  const fetchTopTags = useCallback(async function () {
    MetricService.getTop3Tags().then(setTopTags)
  }, [])

  return {
    topTags,
    fetchTopTags
  }

}