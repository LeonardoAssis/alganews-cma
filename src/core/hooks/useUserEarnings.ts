import { User, UserService } from "leonardoassis-sdk"
import { useCallback, useState } from "react"

export default function useUserEarnings() {
  const [user, setUser] = useState<User.Detailed>()

  const fetchUserEarnings = useCallback(async function () {
    UserService
      .getDetailedUser(7)
      .then(setUser)
  }, [])

  return {
    user,
    fetchUserEarnings
  }
}